package usa.kerby.tk.sjlrmdoclet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.util.ElementScanner9;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.util.DocTrees;

/**
 * @author Trevor Kerby
 * @since Mar 24, 2021
 */

public class ShowTags extends ElementScanner9<Void, Integer> {
	final DocTrees trees;
	final Map<String, Map<String, String>> relations;
	private static final String REL = "rel";
	private static final String DESCRIPTION = "description";
	private static final String USAGE = "usage";

	ShowTags(DocTrees trees) {
		this.trees = trees;
		this.relations = new HashMap<String, Map<String, String>>();
	}

	void show(Set<? extends Element> elements) {
		scan(elements, 0);
	}

	@Override
	public Void scan(Element e, Integer depth) {
		DocCommentTree dcTree = this.trees.getDocCommentTree(e);
		List<? extends AnnotationMirror> annotations = e.getAnnotationMirrors();
		Map<String, String> details = new TreeMap<>();
		if (e.getKind() == ElementKind.ENUM_CONSTANT && dcTree != null) {
			new TagScanner(details).visit(dcTree, null);
			details.put(REL, e.toString().replace("_", "-").toLowerCase());
			annotations.forEach(annotation -> {
				annotation.getElementValues().forEach((i, l) -> {
					details.put(this.getAnnotationNameAsCamelCase(annotation), l.getValue().toString());
				});
			});
			details.put(DESCRIPTION, this.concatenateDocTree(dcTree.getFirstSentence()));
			details.put(USAGE, this.concatenateDocTree(dcTree.getBody()));
			this.relations.put(e.toString(), details);

		}
		return super.scan(e, depth + 1);
	}

	private String concatenateDocTree(List<? extends DocTree> docTree) {
		final StringBuilder builder = new StringBuilder();
		docTree.forEach((x) -> {
			builder.append(x.toString());
		});
		return builder.toString().replace("\n", "\\n");
	}

	private String getAnnotationNameAsCamelCase(AnnotationMirror annotation) {
		String name = annotation.getAnnotationType().asElement().getSimpleName().toString();
		return name.substring(0, 1).toLowerCase() + name.substring(1);
	}

	public Map<String, Map<String, String>> getLinkRelations() {
		return this.relations;
	}
}