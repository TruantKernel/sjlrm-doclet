package usa.kerby.tk.sjlrmdoclet;

import java.util.Map;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.UnknownBlockTagTree;
import com.sun.source.doctree.UnknownInlineTagTree;
import com.sun.source.util.SimpleDocTreeVisitor;

/**
 * @author Trevor Kerby
 * @since Mar 24, 2021
 */
public class TagScanner extends SimpleDocTreeVisitor<Void, Void> {
	private final Map<String, String> tags;

	TagScanner(Map<String, String> tags) {
		this.tags = tags;
	}

	@Override
	public Void visitDocComment(DocCommentTree tree, Void p) {
		return visit(tree.getBlockTags(), null);
	}

	@Override
	public Void visitUnknownBlockTag(UnknownBlockTagTree node, Void p) {
		if (node.getContent().size() > 0) {
			tags.put(node.getTagName(), node.getContent().get(0).toString());
		} else {
			tags.put(node.getTagName(), Boolean.toString(true));
		}
		return null;
	}

	@Override
	public Void visitUnknownInlineTag(UnknownInlineTagTree node, Void p) {
		if (node.getContent().size() > 0) {
			tags.put(node.getTagName(), node.getContent().get(0).toString());
		} else {
			tags.put(node.getTagName(), Boolean.toString(true));
		}
		return null;
	}
}
