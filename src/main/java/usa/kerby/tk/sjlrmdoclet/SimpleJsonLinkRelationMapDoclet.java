package usa.kerby.tk.sjlrmdoclet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic.Kind;

import com.sun.source.util.DocTrees;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import jdk.javadoc.doclet.Doclet;
import jdk.javadoc.doclet.DocletEnvironment;
import jdk.javadoc.doclet.Reporter;

/**
 * @author Trevor Kerby
 * @since Mar 24, 2021
 */
public class SimpleJsonLinkRelationMapDoclet implements Doclet {
	private static final boolean OK = true;

	private static final String OPTION_OUTPUT = "-output";
	private Reporter reporter;
	private OutputOption output;

	public SimpleJsonLinkRelationMapDoclet() {
	}

	@Override
	public void init(Locale locale, Reporter reporter) {
		this.reporter = reporter;
		this.output = new OutputOption(OPTION_OUTPUT, reporter);
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public Set<? extends Option> getSupportedOptions() {
		return Set.of(this.output);
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latest();
	}

	@Override
	public boolean run(DocletEnvironment environment) {
		reporter.print(Kind.NOTE, "Starting SJLRM Doclet...");
		File file;
		if ((file = this.output.getOutput()) != null) {
			FileOutputStream fileOutputStream = null;
			PrintStream printStream = null;
			try {
				fileOutputStream = new FileOutputStream(file);
				printStream = new PrintStream(fileOutputStream);
				this.output(environment, printStream);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (fileOutputStream != null) {
						fileOutputStream.close();
					}
					if (printStream != null) {
						printStream.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			this.output(environment, System.out);
		}
		return OK;
	}

	public boolean output(DocletEnvironment environment, PrintStream stream) {
		Map<String, Map<String, String>> output = this.parse(environment);
		JsonbConfig conf = new JsonbConfig().withFormatting(true);
		Jsonb jsonb = JsonbBuilder.create(conf);
		String json = jsonb.toJson(output);
		stream.print(json);
		return true;
	}

	public Map<String, Map<String, String>> parse(DocletEnvironment environment) {
		DocTrees treeUtils = environment.getDocTrees();
		ShowTags st = new ShowTags(treeUtils);
		Set<Element> targetElements = new HashSet<Element>();
		for (Element e : environment.getSpecifiedElements()) {
			if (!e.getKind().isClass()) {
				continue;
			}
			if (new LinkRelationImplementationScanner().visit(e)) {
				targetElements.add(e);
			}
		}
		st.show(targetElements);
		return st.getLinkRelations();
	}

}
