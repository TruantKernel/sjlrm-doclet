package usa.kerby.tk.sjlrmdoclet;

import java.io.File;
import java.util.List;

import jdk.javadoc.doclet.Reporter;

/**
 * @author Trevor Kerby
 * @since Mar 26, 2021
 */
public class OutputOption extends SjlrmOption {

	File output;

	public OutputOption(String name, Reporter reporter) {
		super(name, "Write output to file at specified path", true, reporter);
	}

	@Override
	public boolean process(String option, List<String> arguments) {
		this.output = new File(arguments.get(0));
		if (output.exists()) {
			if (!output.canWrite()) {
				this.reporter.print(javax.tools.Diagnostic.Kind.ERROR, "Cannot overwrite file at " + output.getAbsolutePath());
				return false;
			}
			if (output.isDirectory()) {
				this.reporter.print(javax.tools.Diagnostic.Kind.ERROR, "Expected file, but a directory was found at " + output.getAbsolutePath());
				return false;
			}
		}
		return true;
	}

	public File getOutput() {
		return this.output;
	}

}
