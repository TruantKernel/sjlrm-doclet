package usa.kerby.tk.sjlrmdoclet;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.SimpleElementVisitor9;

import usa.kerby.tk.jhateoas.http.LinkRelation;

public class LinkRelationImplementationScanner extends SimpleElementVisitor9<Boolean, Boolean> {

	@Override
	public Boolean visitType(TypeElement e, Boolean p) {
		for (TypeMirror mirror : e.getInterfaces()) {
			DeclaredType declaredType = (DeclaredType) mirror;
			final String fullyQualifiedName = declaredType.asElement().toString();
			if (LinkRelation.class.getName().equals(fullyQualifiedName)) {
				return true;
			}
		}
		return false;
	}

}
