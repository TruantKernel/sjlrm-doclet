package usa.kerby.tk.sjlrmdoclet;

import java.util.List;

import jdk.javadoc.doclet.Doclet;
import jdk.javadoc.doclet.Reporter;

/**
 * @author Trevor Kerby
 * @since Mar 24, 2021
 */
public abstract class SjlrmOption implements Doclet.Option {
	private final String name;
	private final boolean hasArg;
	private final String description;
	protected String error;
	protected Reporter reporter;

	SjlrmOption(String name, String description, boolean hasArg, Reporter reporter) {
		this.name = name;
		this.hasArg = hasArg;
		this.description = description;
		this.reporter = reporter;
	}

	@Override
	public int getArgumentCount() {
		return hasArg ? 1 : 0;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Kind getKind() {
		return Kind.STANDARD;
	}

	@Override
	public List<String> getNames() {
		return List.of(name);
	}

	@Override
	public String getParameters() {
		return "";
	}

}
