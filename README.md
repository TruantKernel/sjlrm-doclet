# Simple Json Link Relation Map Doclet
`sjlrm-doclet` is a library that provides a Java Doclet, `SimpleJsonLinkRelationMapDoclet`, to produce a JSON object that maps unregistered [Link Relation](https://datatracker.ietf.org/doc/html/rfc8288) names to details about the respective Link Relation. For a class to be scanned by this doclet, its parent package must be registered as a source and the class itself must implement `LinkRelation` provided by the `jhateoas` [library](https://gitlab.com/073TruantKernel/jhateoas).

# Usage with Maven
configure the maven-javadoc-plugin similar to:

```xml
<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-javadoc-plugin</artifactId>
	<configuration>
		<useStandardDocletOptions>false</useStandardDocletOptions>
		<doclet>usa.kerby.tk.sjlrmdoclet.SimpleJsonLinkRelationMapDoclet</doclet>
		<docletArtifact>
			<groupId>usa.kerby.tk</groupId>
			<artifactId>sjlrm-doclet</artifactId>
			<version>0.0.1-SNAPSHOT</version>
		</docletArtifact>
		<additionalOptions>-output
			${project.basedir}/src/main/resources/META-INF/rels.json</additionalOptions>
		<sourcepath>${project.basedir}/src/main/java/com/example/proj/relations</sourcepath>
	</configuration>
</plugin>
```

where `/src/main/java/com/example/proj/relations` contain a class like:

```java
import usa.kerby.tk.jhateoas.Archetype;
import usa.kerby.tk.jhateoas.ResourceArchetype;
import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * 
 * Unregistered link relations specific to the application
 * 
 */
public enum ApplicationLinkRelation implements LinkRelation {

	/**
	 * Login end-point.
	 * 
	 * 
	 * Send credentials as a POST to this end-point to receive a JWT token upon success
	 * 
	 * 
	 */
	@Archetype(ResourceArchetype.CONTROLLER)
	LOGIN(ApplicationLinkRelation.LOGIN_NAME),

	/**
	 * Logout end-point.
	 * 
	 * Send JWT along with a POST to this end-point to perform a logout with the server
	 * 
	 */
	@Archetype(ResourceArchetype.CONTROLLER)
	LOGOUT(ApplicationLinkRelation.LOGOUT_NAME);

	private static final String CURIE_NAME = "doc:";

	public static final String LOGIN_NAME = "login";
	public static final String LOGOUT_NAME = "logout";

	private final String name;

	private ApplicationLinkRelation(String name) {
		this.name = name;
	}

	public boolean equalsName(String otherName) {
		return name.equals(otherName);
	}

	public String toString() {
		return ApplicationLinkRelation.CURIE_NAME + this.name;
	}

	public String getName() {
		return name;
	}

	public static ApplicationLinkRelation relationValueOf(String name) {
		for (ApplicationLinkRelation e : values()) {
			if (e.getName().equals(name)) {
				return e;
			}
		}
		System.out.println(name + " not found");
		return null;
	}

}
```

and run `sjlrm-doclet` with `mvn run javadoc:javadoc` to produce the following JSON:

```javascript
{
    "LOGOUT": {
        "archetype": "CONTROLLER",
        "description": "Logout end-point.",
        "rel": "logout",
        "usage": "Send credentials as a POST to this end-point to receive a JWT token upon success"
    },
    "LOGIN": {
        "archetype": "CONTROLLER",
        "description": "Login end-point.",
        "rel": "login",
        "usage": "Send JWT along with a POST to this end-point to perform a logout with the server"
    }
}
```

